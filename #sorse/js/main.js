﻿$(function() {

    $('.header__search').click(function() {
        $('.search-form').toggleClass('active')
        $('.project-solutions-nav').slideUp('slow')
        return false
    });
    $('.search-form__clear').click(function() {
        $(this).parents('.search-form').removeClass('active')
    });
    $('.header__search').click(function() {
        $('.header__services').removeClass('active')
        $('.services-nav').slideUp('slow')
        return false
    });
    $('.nav-support').click(function() {
        $('.header__services').removeClass('active')
        $('.services-nav').slideUp('slow')
        return false
    });
    $('.header__services').click(function() {
        $(this).toggleClass('active')
        $('.header__project-solutions').removeClass('active')
        $('.services-nav').slideToggle('slow')
        $('.project-solutions-nav').slideUp('slow')
        return false
    });
    $('.services-nav__close').click(function() {
        $('.header__services').removeClass('active')
        $(this).parents('.services-nav').slideUp('slow')
        return false
    });

    $('.nav-support').click(function() {
        $('.header__project-solutions').removeClass('active')
        $('.header__project-solutions').removeClass('active')
        $('.project-solutions-nav').slideUp('slow')
        return false
    });
    $('.header__project-solutions').click(function() {
        $(this).toggleClass('active')
        $('.project-solutions-nav').slideToggle('slow')
        $('.header__services').removeClass('active')
        $('.services-nav').slideUp('slow')
        return false
    });
    $(document).on('click', function(e) {
        var container = $('.project-solutions-nav');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.slideUp('slow')
            $('.header__project-solutions').removeClass('active');
        }
    });


    $(".input-file input").change(function() {
        var thisFile = $(this).parent().find('.input-file__name');
        var text = 'Файл не выбран';
        thisFile.text(text).removeClass('active');
        if ($(this)[0].files.length == 1) {
            text = $(this)[0].files[0].name;
            thisFile.text(text).addClass('active');
        }

    });

    $(document).on('click', '.popup-close', function() {
        $.fancybox.close();
    })

    $(document).on('touchstart  click', '.popup-close', function() {
        $.fancybox.close();
    })

    $(document).on('mousedown', 'input', function() {
        if ($(this).parent().hasClass('error')) {
            $(this).parent().removeClass('error')
        }
    })


    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 100, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
        scrollContainer: null // optional scroll container selector, otherwise use window
    });
    wow.init();

    if ($(window).width() < 1200) {
        $('[data-wow-delay]').each(function() {
            $(this).addClass('animation-delay-none')
        })
    }



    $('.burger').click(function() {
        $(this).toggleClass('active');
        $('.mobile-nav').slideToggle('slow');
    });

    $('.mobile-nav__services h3').click(function() {
        $(this).toggleClass('serv-active').parent().find('ul').slideToggle('slow');
    });

    $('div.mobile-nav__title').click(function() {
        $(this).parent('.mobile-nav__item').toggleClass('active');
        $(this).parent().find('.mobile-nav__hidden').slideToggle('slow');
    });

    $('div.news-filter__title').click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active')
            $(this).parent().find('.news-filter__body').slideUp('slow');
        }
        else {
            $('div.news-filter__title').removeClass('active');
            $('.news-filter__body').slideUp('slow');
            $(this).toggleClass('active').parent().find('.news-filter__body').slideToggle('slow');
            if ($('.s-news-list__filter').hasClass('active')) {
                setTimeout(() => {
                    $('.s-news-list__filter').removeClass('active');
                }, 250);
                setTimeout(() => {
                    $(this).closest('.s-news-list__filter').addClass('active');
                }, 251);
            }
            else {
                $(this).closest('.s-news-list__filter').addClass('active');
            }
        }
    });

    $('.news-filter__item').click(function() {
        var val = $(this).text()
        $(this).parent().slideUp('slow').parent().removeClass('active').find('.news-filter__title').removeClass('active').text(val);
      });
    
    $(".news-filter__item").click(function(){
        $(this).toggleClass('active').siblings().removeClass('active');
    });

    $('.s-about_top__item a').click((e) =>{
        e.preventDefault();
        let scrollClass = e.target.getAttribute('data-scroll')
        let scroll = document.getElementsByClassName(scrollClass)
        $('html, body').animate({
            scrollTop: $(scroll[0]).offset().top - 78
        }, 2000);
    })

    $('.main-ban_tabs-top a').click((e) =>{
        e.preventDefault();
        let scrollClass = e.target.getAttribute('data-scroll')
        let scroll = document.getElementsByClassName(scrollClass)
        $('html, body').animate({
            scrollTop: $(scroll[0]).offset().top - 78
        }, 2000);
    })

    $('.mobile-nav__hidden a').click((e) =>{
        if (e.target.hasAttribute('data-scroll')) {
            e.preventDefault();
            $(e.target).parents('.mobile-nav__hidden').slideToggle('slow');
            $(e.target).parents('.mobile-nav__item').removeClass('active');
            let scrollClass = e.target.getAttribute('data-scroll');
            let scroll = document.getElementsByClassName(scrollClass);
            let navHeight = $('.mobile-nav__title').outerHeight(); 
            if ($('body').hasClass('fixed-top')) {
                $('html, body').animate({
                    scrollTop: $(scroll[0]).offset().top - (navHeight + 30)
                }, 2000);
            } else {
                $('html, body').animate({
                    scrollTop: $(scroll[0]).offset().top - navHeight * 6
                }, 2000);
            }
        }
    })

    $('.s-base_group a').click((e) =>{
        e.preventDefault();
        let scrollClass = e.target.getAttribute('data-scroll')
        let scroll = document.getElementsByClassName(scrollClass)
        $('html, body').animate({
            scrollTop: $(scroll[0]).offset().top - 78
        }, 2000);
    })
});

$('.js-social-btn').click(function(){
    if ($(this).next('.js-social-wrapper').hasClass('active')) {
        $('.js-social-wrapper').removeClass('active')
    }
    else {
        $('.js-social-wrapper').removeClass('active')
        $(this).next('.js-social-wrapper').addClass('active')
    }
})
$(document).click(function(e){
    let clickTarget = $(e.target).closest('.js-social-wrapper')
    if (!$(clickTarget).hasClass('js-social-wrapper') && (!$(e.target).hasClass('js-social-btn'))) {
        $('.js-social-wrapper').removeClass('active')
    }
})


/*
    $("input[name=phone]").mask("+7 (999) 999-99-99");

    var  wheihht=96;
    $(window).scroll(function(){
        var bo = $(this).scrollTop();
        if ( bo >= wheihht) {$('nav').addClass('scr_nav')};
            if ( bo < wheihht) {$('nav').removeClass('scr_nav')};
    })
    $("nav li a").click(function(){
        var target = $(this).attr('href');
            $('html, body').animate({scrollTop: $(target).offset().top-20}, 1300);
            return false;
    });

*/

/*-----------Импорт внешних JS файлов-----------*/
// @prepros-append external/dropzone.min.js
// @prepros-append external/jquery.overlayScrollbars.js
// @prepros-append external/mediaHandler.js
// @prepros-append external/tooltipster.bundle.min.js

/*-----------Импорт внутренних JS файлов-----------*/
// @prepros-append internal/dragAndDrop.js
// @prepros-append internal/scroll.js
// @prepros-append internal/tabs.js
// @prepros-append internal/like-counter.js
// @prepros-append internal/calculator.js
// @prepros-append internal/accordion.js
// @prepros-append internal/map.js
// @prepros-append internal/tooltip.js
// @prepros-append internal/sliders.js
// @prepros-append internal/forms.js
// @prepros-append internal/video.js