function MediaHandler(queriesList, func) {
  if (typeof func !== 'function') {
      func = function (f) {
          if (typeof f === 'function') {
              f();
          }
      };
  }
  var defaultQuery;
  var queryList;
  var defaultValue = queriesList.default;
  var queries = $.extend({}, queriesList);
  delete queries.default;
  defaultQuery = {query: 'default', value: defaultValue};
  queryList = Object.entries(queries)
      .map(function (arr) {
          var query = arr[0];
          var value = arr[1];
          return {list: window.matchMedia(query), value: value, query: query};
      });
  queryList.forEach(function (object) {
      object.list.addListener(setCurrentQuery);
  });
  setCurrentQuery();
  this.destroy = function () {
      queryList.forEach(function (object) {
          object.list.removeListener(setCurrentQuery);
      });
      queryList = null;
  };

  function setCurrentQuery() {
      var current = queryList.filter(function (object) {
          return object.list.matches;
      })[0] || defaultQuery;
      func(current.value, current.query);
  }
}