$(function configurate () {
  let decrease = $('.js-configurator__remove');
  let increase = $('.js-configurator__add');
  let resetBtn = $('.js-configurator__reset');
  let allConfigurators = $('.js-configurator');
  $(allConfigurators).each(function() {
    count(this);
  })

  $(increase).on('click', function () {
      let configurator = this.closest('.js-configurator');
      let line = this.closest('.s-configurator__line');
      let counting = $(line).find('.js-configurator__counting');
      let price = Number($(line).find('.js-configurator__price')[0].textContent);
      let sum = Number($(line).find('.js-configurator__sum')[0].textContent);
      
      counting[0].textContent++
      if (price % 1 !== 0) 
        sum = (sum + price).toFixed(2);
      else
        sum = (sum + price)
      $(line).find('.js-configurator__sum')[0].textContent = sum
      count(configurator)
      $($(line).find('.js-configurator__remove')[0]).removeClass('opacity')
  })

  $(decrease).on('click', function () {
      let configurator = this.closest('.js-configurator');
      let line = this.closest('.s-configurator__line');
      let counting = $(line).find('.js-configurator__counting');
      let price = Number($(line).find('.js-configurator__price')[0].textContent);
      let sum = Number($(line).find('.js-configurator__sum')[0].textContent);
      if (counting[0].textContent > 0) {
          counting[0].textContent--
          if (price % 1 !== 0) {
              if (counting[0].textContent == 0) {
                sum = (sum - price).toFixed(0)
              }
              else {
                sum = (sum - price).toFixed(2)
              }
          }
          else
            sum = (sum - price)
          $(line).find('.js-configurator__sum')[0].textContent = sum
          count(configurator)
      }
      if (counting[0].textContent == 0) {
        $($(line).find('.js-configurator__remove')[0]).addClass('opacity')
    }
  })

  $(resetBtn).on('click', function () {
      let configurator = this.closest('.js-configurator');
      let allCounting = $(configurator).find('.js-configurator__counting');
      let total = $(configurator).find('.js-configurator__total');
      let allSum = $(configurator).find('.js-configurator__sum')
      for (let i = 0; i < allCounting.length; i++) {
          allCounting[i].textContent = 0
      }
      for (let j = 0; j < allSum.length; j++) {
          allSum[j].textContent = 0
      }
      count(configurator)
      $(configurator).find('.js-configurator__yes').removeClass('active');
      $(configurator).find('.js-configurator__no').addClass('active');
      $(configurator).find('.js-configurator__remove').addClass('opacity')
  })

  $('.js-configurator__yes').on('click', function () {
      let configurator = this.closest('.js-configurator');
      if (!$(this).hasClass('active')) {
          $(this).addClass('active');
          $('.js-configurator__no').removeClass('active');
          let line = this.closest('.s-configurator__line');
          $(line).find('.js-configurator__sum')[0].textContent = $(line).find('.js-configurator__price')[0].textContent;
          count(configurator)
      }
  })
  
  $('.js-configurator__no').on('click', function () {
      let configurator = this.closest('.js-configurator');
      if (!$(this).hasClass('active')) {
          $(this).addClass('active');
          $('.js-configurator__yes').removeClass('active');
          let line = this.closest('.s-configurator__line');
          $(line).find('.js-configurator__sum')[0].textContent = 0;
          count(configurator)
      }
  })

  function count (configurator) {
      let total = $(configurator).find('.js-configurator__total');
      let minTotal = $(configurator).find('.js-configurator__min-total');
      total[0].textContent = 0;
      let allSum = $(configurator).find('.js-configurator__sum')
      let minSum = $(configurator).find('.js-configurator__min-sum')
      for (let i = 0; i < allSum.length; i++) {
          total[0].textContent = Number(total[0].textContent) + Number(allSum[i].textContent.replace(/ /g, ""))
      }
      total[0].textContent = numberWithSpaces(Number(total[0].textContent).toFixed(2))
      if (minTotal.length !== 0) {
        minTotal[0].textContent = 0;
        for (let i = 0; i < minSum.length; i++) {
          minTotal[0].textContent = Number(minTotal[0].textContent) + Number(minSum[i].textContent.replace(/ /g, ""))
        }
        minTotal[0].textContent = numberWithSpaces(Number(minTotal[0].textContent).toFixed(2))
      }
  }
  
  function numberWithSpaces(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }
})
