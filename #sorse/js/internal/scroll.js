$(function () {
  var options2 = {
      overflowBehavior: {
          x: 'scroll',
          y: 'hidden',
          clipAlways: false
      },
      scrollbars : {
          visibility: 'hidden',
      }
  };
  $('.js-scroll-x').overlayScrollbars(options2);
})

$(function() {
    let navBlocks = []
    let navLinks = $('.main-ban_tabs-top.fixed a')
    let indexShown
    $(navLinks).each(function() {
        navBlocks.push(this.getAttribute('data-scroll'))
    })
    function showingListen() {
        let wt = $(window).scrollTop();
        let wh = $(window).height();
        for (let i = 0; i < navBlocks.length; i++) {
            let et = $('.' + navBlocks[i]).offset().top;
            let eh = $('.' + navBlocks[i]).outerHeight();
            if (wt + wh >= et && wt + wh - eh * 2 <= et + (wh - eh)) {
                if (!$('.main-ban_tabs-top.fixed .main-ban_tabs__item').hasClass('shown')) {
                    $('.main-ban_tabs-top.fixed [data-scroll=' + navBlocks[i] + ']').closest('.main-ban_tabs__item').addClass('shown');
                    indexShown = i
                } else {
                    if (i < indexShown) {
                        $('.main-ban_tabs__item').removeClass('shown')
                        $('.main-ban_tabs-top.fixed [data-scroll=' + navBlocks[i] + ']').closest('.main-ban_tabs__item').addClass('shown');
                    }
                }
            } else {
                $('.main-ban_tabs-top.fixed [data-scroll=' + navBlocks[i] + ']').closest('.main-ban_tabs__item').removeClass('shown');
            }
        }
    }
    showingListen()
    $(window).scroll(function() {
        showingListen()
    })
})
$(function() {
    if ($('.main-ban_tabs.js-scroll-x').length) {
        let ww = $(window).width();
        let linkCoordinates = $('.main-ban_tabs .main-ban_tabs__item.active').offset().left;
        let linkWidth = $('.main-ban_tabs .main-ban_tabs__item.active').outerWidth();
        let scrollBlock = $('.main-ban_tabs')[0];
        let instance = OverlayScrollbars(scrollBlock)
        instance.scroll((linkCoordinates - 0.5 * (ww - linkWidth)), 0);
    }
})