function dragAndDrop () {
  let instance = $('.js-drop-zone'),
      input = instance.find('input');

  input.on('click', (e) => { e.preventDefault()})
  instance.dropzone({ 
    url: '/file/post',
    previewTemplate: `
    <div class="drop-zone-file">
      <div class="drop-zone-file__name">
        <span data-dz-name></span>
      </div>
      <div class="drop-zone-file__btn" data-dz-remove>
        x
      </div>
    </div>
    `,
    init: function() {
      let message = instance.find('.js-drop-zone-message')
      this.on("addedfile", function(file) { 
        if (!message.hasClass('hidden')) {
          message.addClass('hidden')
        }
      });
      this.on("reset", function(file) { 
        if (message.hasClass('hidden')) {
          message.removeClass('hidden')
        }
      });
    }
  });
  
}
dragAndDrop();