$(function () {
  let instance = $('.js-video'),
      popupVideo = $('.js-popup-video-frame');
  
  instance.on('click', function () {
      let video = $(this).data('video');
      popupVideo.attr('src', `https://www.youtube.com/embed/${video}?autoplay=1`)
  });
});

$(function () {
  let placeholder = $('.js-video-placeholder');
  placeholder.on('click', function () {
    let instance = $(this).closest('.js-video-player'),
        frame = instance.find('iframe'),
        video = frame.data('video');
    frame.attr('src', `https://www.youtube.com/embed/${video}?autoplay=1`)
    $(this).hide()
    if($('.video-slider')) {
      $('.video-slider').on('swipe', function(event, slick, direction){
        frame.attr('src', `https://www.youtube.com/embed/${video}`)
      });
    }
  })
  
});