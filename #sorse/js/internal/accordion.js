var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            panel.classList.remove("active");
            // panel.style.padding = 0;
        } else {
            panel.classList.add("active");
            panel.style.maxHeight = panel.scrollHeight + "px";
            // panel.style.padding = '10px 18px';
            // panel.style.paddingLeft = '80px';
        }
    });
};
$(function() {
    if ($('.js-accordion-btn--iaas').length) {
        let first_accordion = $('.js-accordion-btn--iaas').filter('.active').closest('.s-scenario_list__item')[0];
        let first_content_height = $(first_accordion).find('.js-accordion-content--iaas').outerHeight();
        first_accordion.style.paddingBottom = first_content_height + 'px'
        $('.js-accordion-btn--iaas').click(function() {
            if (!$(this).hasClass('active')) {
                let accordion = this.closest('.s-scenario_list__item');
                let accordion_content = $(accordion).find('.js-accordion-content--iaas');
                let accordion_head = $(accordion).find('.s-scenario_list__item___title');
                $('.s-scenario_list__item').each(function() {
                    this.style.paddingBottom = null;
                })
                $('.js-accordion-btn--iaas').removeClass('active');
                $('.s-scenario_list__item___title').removeClass('active');
                $('.js-accordion-content--iaas').removeClass('active');
                $(accordion_content).addClass('active');
                $(accordion_head).addClass('active');
                $(this).addClass('active');
                let content_height = $(accordion_content).outerHeight();
                accordion.style.paddingBottom = content_height + 'px';
            } else {
                new MediaHandler({
                    default: function () {
                        $('.s-scenario_list__item').each(function() {
                            this.style.paddingBottom = null;
                        })
                        $('.js-accordion-btn--iaas').removeClass('active');
                        $('.s-scenario_list__item___title').removeClass('active');
                        $('.js-accordion-content--iaas').removeClass('active');
                    },
                    '(min-width: 951px)': function () {
                    }
                })
            }
        })
    }
})
