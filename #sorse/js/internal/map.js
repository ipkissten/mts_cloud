$(function() {
  let instance  = $('#map');

  if (instance.length > 0) {
      ymaps.ready(init);
      function init(){
          var myMap = new ymaps.Map("map", {
              center: [55.691755, 37.660527],
              zoom: 16
          });
          myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
              hintContent: 'CloudMTS',
          },
          {
              iconLayout: 'default#image',
              iconImageHref: '../img/contacts/location.svg',
              iconImageSize: [35, 44],
              iconImageOffset: [-5, -38]
          })
          myMap.geoObjects.add(myPlacemark)
      }
  }
})