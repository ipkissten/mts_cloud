const btns = $('.events_like');
$(btns).on('click', function() {
    if (!$(this).hasClass('liked')) {
        let likeCount = $(this).children('.events_like-counter').text()
        likeCount++
        $(this).children('.events_like-counter').text(likeCount)
        $(this).addClass('liked')
    }
    else {
        let likeCount = $(this).children('.events_like-counter').text()
        likeCount--
        $(this).children('.events_like-counter').text(likeCount)
        $(this).removeClass('liked')
    }
})

