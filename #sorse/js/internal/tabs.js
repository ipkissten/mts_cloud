$(function() {
  let instance = $('.main-ban_tabs-top');

  if (instance.length > 0) {
    let fixed = false
    let wt = $(window).scrollTop();
    let wh = $(window).height();
    let et
    let eh
    new MediaHandler({
        default: function () {
            et = $('.main-ban').offset().top;
            eh = $('.main-ban').outerHeight();
        },
        '(min-width: 951px)': function () {
            et = $('.breadcrumbs').offset().top;
            eh = $('.breadcrumbs').outerHeight();
        }
    })
    let dh = $(document).height();

    $(document).ready(function() {
        let wt = $(window).scrollTop();
        if(wt > et+eh) {
            fixed = true
        }
        if(wt < et+eh) {
            fixed = false
        }
        if(fixed){
            $('body').addClass('fixed-top');
        } else {
            $('body').removeClass('fixed-top');
        }
    });

    $(window).scroll(function(){
        let wt = $(window).scrollTop();
        if(wt > et-eh){
            fixed = true
        }

        if(wt < et+eh) {
            fixed = false
        }
        if(fixed){
            $('body').addClass('fixed-top');
        } else {
            $('body').removeClass('fixed-top');
        }
    });
  }

});

$('.select-filter__title').click(function() {
  $(this).parent().toggleClass('active').find('.select-filter__body').slideToggle('slow').parents('.price-configurator-filter__item').toggleClass('active')
});

$('.select-filter__item').click(function() {
  var val = $(this).text()
  $(this).parent().slideUp('slow').parent().removeClass('active').find('.select-filter__title').removeClass('active').text(val).parents('.price-configurator-filter__item').removeClass('active');
});

$('.js-tab-trigger').click(function () {
  var id = $(this).attr('data-tab');
  var content = $('.js-tab-content[data-tab="'+ id +'"]');
  var tabHead = $('.js-tab-trigger[data-tab="'+ id +'"]');
  $('.select-filter__title').text(tabHead.filter('.select-filter__item').text())

  $('.js-tab-trigger.active').removeClass('active');
  tabHead.addClass('active');

  $('.js-tab-content.active').removeClass('active');
  content.addClass('active');
  content.children('.clients-slider--gru').slick('refresh');
  content.children('.functional-slider').slick('refresh')
});